.HOME
=====

**!!! Personal usage only !!! Use at your own risk !!!**

Eventually it'll become real help for bootstrapping a new box.
Atm it's a way to preserve tricks'n'hacks regarding *nix shells, text configs, etc.

Usage: ``git clone git@gitlab.com:theoden-dd/dothome.git .home``

Inspired by https://github.com/klen/.home